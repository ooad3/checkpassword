const { checkLength, checkAlphabet,checkDigit,checkPassword, checkSymbol } = require('./password')
describe('Test Password Length', () => {
  test("should 8 charecters to be true", () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test("should 7 charecters to be false", () => {
    expect(checkLength('1234567')).toBe(false)
  })
  test("should 25 charecters to be true", () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test("should 26 charecters to be false", () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})  

describe("Test Alphabet", () => {
  test('should has alphabet m in  password', () => {
    expect(checkAlphabet('m')).toBe(true)
  });

  test('should has alphabet A in  password', () => {
    expect(checkAlphabet('A')).toBe(true)
  });

  test('should has not alphabet in  password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe("Test Digit", () => {
  test('should has digit 1 in  password', () => {
    expect(checkDigit('1')).toBe(true)
  })

  test('should has digit in  password', () => {
    expect(checkDigit('sumsung')).toBe(false)
  })

  test('should has digit in password', () => {
    expect(checkDigit('131111111')).toBe(true)
  })
})

describe("Test Symbol", () => {
  test('should has symbol ! in  password', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })

  test('should has symbol @ in  password', () => {
    expect(checkSymbol('1@sun')).toBe(true)
  })

  test('should has symbol in  password', () => {
    expect(checkSymbol('sun')).toBe(false)
  })
})

describe("Test Password", () => {
  test('should password cde@1234 to be true', () => {
    expect(checkPassword('cde@1234')).toBe(true);
  })

  test('should password Itt@1 to be true', () => {
    expect(checkPassword('Itt@1')).toBe(false);
  })

  test('should password #1122 to be true', () => {
    expect(checkPassword('#1122')).toBe(false);
  })

  test('should password sum@sung to be true', () => {
    expect(checkPassword('sum@sung')).toBe(false);
  })

  test('should password SungSung1234 to be true', () => {
    expect(checkPassword('SungSung1234')).toBe(false);
  })
})